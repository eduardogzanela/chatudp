package com.br.batepapo.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.br.batepapo.model.Usuario;
import com.br.batepapo.udp.client.UdpClient;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.Duration;

public class LoginController implements Initializable{

	@FXML
	private TextField passField;
	@FXML
	private TextField userField;
	@FXML
	private Button loginButton;
	@FXML
	private Label errorLabel;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//Focus on user Field when open app
		userField.requestFocus();
	}
	
	public void validateInputCallLogin(ActionEvent event) throws IOException, ClassNotFoundException {
		if(userField.getText().isEmpty() || passField.getText().isEmpty()) {
			errorLabel.setText("Usuario e Senha s�o obrigatorios");
			errorLabel.setVisible(true);
			PauseTransition visiblePause = new PauseTransition(
			        Duration.seconds(3)
			);
			visiblePause.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						errorLabel.setVisible(false);
						
					}
				}
			);
			visiblePause.play();
			return;
		}
		Usuario user = new Usuario(userField.getText(), passField.getText());
		login(user);
	}
	
	
	public void login(Usuario user) throws IOException, ClassNotFoundException {
		UdpClient client = new UdpClient();
		int status = client.login(user);
		if(status == 200) {
			errorLabel.setText("Logado com sucesso");
		}
		client.close();
	}
	

}
