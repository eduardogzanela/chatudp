package com.br.batepapo.udp.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.br.batepapo.model.UdpRequest;
import com.br.batepapo.model.UdpResponse;
import com.br.batepapo.model.Usuario;

public class UdpClient {
    private static final int PORT = 4445;
	private DatagramSocket socket;
    private InetAddress address;
 
    private byte[] buf;
    private byte[] receiveBuf = new byte[256];
    
    public UdpClient() throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName("localhost");
    }
 
    public int login(Usuario user) throws IOException, ClassNotFoundException {
	    //Create request
    	UdpRequest request = new UdpRequest();
	    request.setType("login");
	    request.setObject(user);
	    
	    //Convert the object to Byte Array and attribute to byte
    	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    	ObjectOutputStream os = new ObjectOutputStream(outputStream);
    	os.writeObject(request);
    	buf = outputStream.toByteArray();
       	
    	//Create DatagramPacket and send to server
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, PORT);
        socket.send(packet);
        
        //Create a response DatagramPacket and wait for server response
        DatagramPacket receivePacket = new DatagramPacket(receiveBuf, receiveBuf.length);
        socket.receive(receivePacket);
        
        //Read and convert response to UDPReponse
        ByteArrayInputStream in = new ByteArrayInputStream(receivePacket.getData());
        ObjectInputStream is = new ObjectInputStream(in);
        UdpResponse response = (UdpResponse) is.readObject();
        
        //Return the status response
        return response.getStatus();
    }
 
    public void close() {
        socket.close();
    }
}