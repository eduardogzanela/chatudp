package com.br.batepapo.model;

import java.io.Serializable;

public class Usuario implements Serializable {

	public Usuario(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	private static final long serialVersionUID = 1L;
	private String email;
	private String password;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		return "Email = " + getEmail();
	}
}