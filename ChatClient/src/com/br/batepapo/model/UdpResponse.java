package com.br.batepapo.model;

import java.io.Serializable;

public class UdpResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int status;
	private Object object;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
}
