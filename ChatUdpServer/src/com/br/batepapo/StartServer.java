package com.br.batepapo;

import com.br.batepapo.udp.UDPServer;

public class StartServer {

	public static void main(String[] args) {
		UDPServer updServer = new UDPServer();
		updServer.start();
	}

}
