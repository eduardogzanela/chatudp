package com.br.batepapo.udp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import com.br.batepapo.model.UdpRequest;
import com.br.batepapo.model.UdpResponse;
import com.br.batepapo.model.Usuario;

public class UDPServer extends Thread {
	 
    private DatagramSocket socket;
    private boolean running;
    private int port = 4445;
    private byte[] buf = new byte[256];
    private byte[] respBuf = new byte[256];
 
    public UDPServer() {
        try {
			socket = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
    }
 
    public void run() {
        running = true;
        System.out.println("UDP server start running on port: " + port);
        try {
	        while (running) {
	        	//Create DatagramPacket and wait for request
	        	DatagramPacket packet  = new DatagramPacket(buf, buf.length); 
	        	socket.receive(packet);
	            
	        	//Get Port and Address from client
	            InetAddress address = packet.getAddress();
	            int port = packet.getPort();
	            
	            //To finish server remotely 
	            String received = new String(packet.getData(), 0, packet.getLength());
	            if (received.equals("end")) {
	                running = false;
	                continue;
	            }
	            
	            //Convert request to UdpRequest object
	            ByteArrayInputStream in = new ByteArrayInputStream(packet.getData());
	            ObjectInputStream is = new ObjectInputStream(in);
	            UdpRequest request = (UdpRequest) is.readObject();
	            
	            //Cria objeto de resposta
	            UdpResponse response = new UdpResponse();
	            
	            //Verifica o tipo de requisi��o neste caso login ent�o obtem objeto usuario do login
	            if(request.getType().equalsIgnoreCase("login")) {
	            	Usuario usuario = (Usuario) request.getObject();
	            	System.out.println("Objeto login usuario recebido \n"+usuario);
	            	//Poderia chamar a logica pra ver se existe usuario
	            	//Se usuario existe seta status 200
	            	response.setStatus(200);
	            } else {
	            	response.setStatus(404);
	            }
	            	            
	            //Converte objeto de reposta para byte
	            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	        	ObjectOutputStream os = new ObjectOutputStream(outputStream);
	        	os.writeObject(response);
	        	respBuf = outputStream.toByteArray();
	            
	        	//Create DatagramPacket and send the response
	        	DatagramPacket responsePacket = new DatagramPacket(respBuf, respBuf.length, address, port);
	            socket.send(responsePacket);
	    			
	        }
	        socket.close();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}