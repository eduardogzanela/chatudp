package com.br.batepapo.model;

import java.io.Serializable;

public class UdpRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String type;
	private Object object;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	
}
